/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.chat.dao;

import com.skyeye.chat.entity.TalkChatHistory;
import com.skyeye.eve.dao.SkyeyeBaseMapper;

/**
 * @ClassName: TalkChatHistoryDao
 * @Description: 聊天历史记录数据层
 * @author: skyeye云系列--卫志强
 * @date: 2025/1/12 14:24
 * @Copyright: 2025 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface TalkChatHistoryDao extends SkyeyeBaseMapper<TalkChatHistory> {

}
