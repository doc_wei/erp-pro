/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.eve.dao;

import com.skyeye.eve.entity.model.SysEveModel;

/**
 * @ClassName: SysEveModelDao
 * @Description: 素材管理数据层
 * @author: skyeye云系列
 * @date: 2021/11/14 9:03
 * @Copyright: 2021 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface SysEveModelDao extends SkyeyeBaseMapper<SysEveModel> {

}
