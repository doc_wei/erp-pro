package com.skyeye.office.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.office.entity.DocumentComment;

public interface DocumentCommentDao extends SkyeyeBaseMapper<DocumentComment> {
}
