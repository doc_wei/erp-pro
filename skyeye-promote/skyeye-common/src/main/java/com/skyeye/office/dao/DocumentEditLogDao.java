package com.skyeye.office.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.office.entity.DocumentEditLog;

public interface DocumentEditLogDao extends SkyeyeBaseMapper<DocumentEditLog> {
}
