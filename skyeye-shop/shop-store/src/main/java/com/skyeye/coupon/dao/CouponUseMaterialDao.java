/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.coupon.dao;

import com.skyeye.coupon.entity.CouponUseMaterial;
import com.skyeye.eve.dao.SkyeyeBaseMapper;

/**
 * @ClassName: CouponUseMaterialDao
 * @Description: 优惠券领取适用商品对象数据层
 * @author: skyeye云系列--卫志强
 * @date: 2024/9/8 10:39
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface CouponUseMaterialDao extends SkyeyeBaseMapper<CouponUseMaterial> {
}
