/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.order.service;

import com.skyeye.base.business.service.SkyeyeBusinessService;
import com.skyeye.common.object.InputObject;
import com.skyeye.common.object.OutputObject;
import com.skyeye.order.entity.OrderComment;
import com.skyeye.order.entity.OrderItem;

import java.util.List;

/**
 * @ClassName: OrderCommentService
 * @Description: 商品订单评论管理
 * @author: skyeye云系列--卫志强
 * @date: 2024/9/8 10:39
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface OrderCommentService extends SkyeyeBusinessService<OrderComment> {
    void queryOrderCommentPageList(InputObject inputObject, OutputObject outputObject);

    List<OrderComment> queryListByOrderItemIdAndType(List<String> orderItemIds, Integer key);

    void queryMyOrderCommentList(InputObject inputObject, OutputObject outputObject);
}
