package com.skyeye.exam.examancheckbox.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.skyeye.annotation.service.SkyeyeService;
import com.skyeye.base.business.service.impl.SkyeyeBusinessServiceImpl;
import com.skyeye.common.constans.CommonConstants;
import com.skyeye.common.object.InputObject;
import com.skyeye.common.object.OutputObject;
import com.skyeye.common.util.mybatisplus.MybatisPlusUtil;
import com.skyeye.exam.examancheckbox.dao.ExamAnCheckboxDao;
import com.skyeye.exam.examancheckbox.entitiy.ExamAnCheckbox;
import com.skyeye.exam.examancheckbox.service.ExamAnCheckboxService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: ExamAnCheckboxServiceImpl
 * @Description: 答卷 多选题保存表服务层
 * @author: skyeye云系列--lqy
 * @date: 2024/7/19 11:01
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */

@Service
@SkyeyeService(name = "答卷 多选题保存表", groupName = "答卷 多选题保存表")
public class ExamAnCheckboxServiceImpl extends SkyeyeBusinessServiceImpl<ExamAnCheckboxDao, ExamAnCheckbox> implements ExamAnCheckboxService{

    @Override
    public void queryExamAnCheckboxListById(InputObject inputObject, OutputObject outputObject) {
        Map<String, Object> map = inputObject.getParams();
        String id = map.get("id").toString();
        QueryWrapper<ExamAnCheckbox> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(CommonConstants.ID, id);
        List<ExamAnCheckbox> examAnCheckboxList = list(queryWrapper);
        outputObject.setBean(examAnCheckboxList);
        outputObject.settotal(examAnCheckboxList.size());
    }

    @Override
    public List<ExamAnCheckbox> slectBySurveyId(String surveyId) {
        QueryWrapper<ExamAnCheckbox> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(MybatisPlusUtil.toColumns(ExamAnCheckbox::getBelongId), surveyId);
        return list(queryWrapper);
    }

    @Override
    public List<ExamAnCheckbox> selectAnCheckBoxByQuId(String id) {
        QueryWrapper<ExamAnCheckbox> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(MybatisPlusUtil.toColumns(ExamAnCheckbox::getQuId), id);
        return list(queryWrapper);
    }
}
