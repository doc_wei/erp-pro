package com.skyeye.exam.examanchencheckbox.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.exam.examananswer.entity.ExamAnAnswer;
import com.skyeye.exam.examanchencheckbox.entity.ExamAnChenCheckbox;

/**
 * @ClassName: ExamAnChenCheckboxDao
 * @Description: 答卷 矩阵多选题接口层
 * @author: skyeye云系列--lqy
 * @date: 2024/7/16 11:01
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */

public interface ExamAnChenCheckboxDao extends SkyeyeBaseMapper<ExamAnChenCheckbox> {
}
