package com.skyeye.exam.examancompchenradio.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.exam.examancompchenradio.entity.ExamAnCompChenRadio;

/**
 * @ClassName: ExamAnCompChenRadioDao
 * @Description: 答卷 复合矩阵单选题数据接口层
 * @author: skyeye云系列--lqy
 * @date: 2024/7/19 11:01
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */

public interface ExamAnCompChenRadioDao extends SkyeyeBaseMapper<ExamAnCompChenRadio> {
}
