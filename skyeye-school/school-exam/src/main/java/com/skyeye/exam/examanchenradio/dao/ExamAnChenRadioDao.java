package com.skyeye.exam.examanchenradio.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.exam.examanchenradio.entity.ExamAnChenRadio;

/**
 * @ClassName: ExamAnChenRadioDao
 * @Description: 答卷 矩阵单选题数据接口层
 * @author: skyeye云系列--lqy
 * @date: 2024/7/16 11:01
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */

public interface ExamAnChenRadioDao extends SkyeyeBaseMapper<ExamAnChenRadio> {
}
