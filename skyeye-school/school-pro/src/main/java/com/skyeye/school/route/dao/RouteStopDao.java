package com.skyeye.school.route.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.route.entity.RouteStop;

/**
 * @ClassName: RouteStopDap
 * @Description: 路线站点数据层
 * @author: skyeye云系列--lqy
 * @date: 2024/12/1 14:35
 * @Copyright: 2023 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface RouteStopDao extends SkyeyeBaseMapper<RouteStop> {
}
