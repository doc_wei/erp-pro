package com.skyeye.school.chat.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.chat.entity.ChatHistory;

public interface ChatHistoryDao extends SkyeyeBaseMapper<ChatHistory> {
}
