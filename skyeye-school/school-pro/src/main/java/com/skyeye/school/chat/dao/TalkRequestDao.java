package com.skyeye.school.chat.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.chat.entity.TalkRequest;

public interface TalkRequestDao extends SkyeyeBaseMapper<TalkRequest> {
}
