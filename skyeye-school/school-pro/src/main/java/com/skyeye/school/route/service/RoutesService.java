package com.skyeye.school.route.service;

import com.skyeye.base.business.service.SkyeyeBusinessService;
import com.skyeye.common.object.InputObject;
import com.skyeye.common.object.OutputObject;
import com.skyeye.school.route.entity.Routes;

/**
 * @ClassName: RoutesService
 * @Description: 路线服务数据接口层
 * @author: skyeye云系列--lqy
 * @date: 2024/7/18 11:01
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface RoutesService extends SkyeyeBusinessService<Routes> {

    void queryRoutesByStartAndEnd(InputObject inputObject, OutputObject outputObject);

    void queryPageListBySchoolId(InputObject inputObject, OutputObject outputObject);

    void queryRoutesNavigationLists(InputObject inputObject, OutputObject outputObject);
}
