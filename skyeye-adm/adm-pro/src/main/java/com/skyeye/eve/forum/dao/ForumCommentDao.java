package com.skyeye.eve.forum.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.forum.entity.ForumComment;

public interface ForumCommentDao extends SkyeyeBaseMapper<ForumComment> {
}
