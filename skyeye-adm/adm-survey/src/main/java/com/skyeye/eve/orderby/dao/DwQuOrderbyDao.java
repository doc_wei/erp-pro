package com.skyeye.eve.orderby.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.orderby.entity.DwQuOrderby;

public interface DwQuOrderbyDao extends SkyeyeBaseMapper<DwQuOrderby> {
}
