package com.skyeye.videocomment.dao;

import com.skyeye.common.entity.search.CommonPageInfo;
import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.videocomment.entity.VideoComment;


/**
 * @ClassName: VideoCommentDao
 * @Description: 视频评论数据层
 * @author: skyeye云系列--lqy
 * @date: 2024/3/9 14:31
 * @Copyright: 2023 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface VideoCommentDao extends SkyeyeBaseMapper<VideoComment> {

}
