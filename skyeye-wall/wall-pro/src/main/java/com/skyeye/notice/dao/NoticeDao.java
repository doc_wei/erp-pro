package com.skyeye.notice.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.notice.entity.Notice;

/**
 * @ClassName: NoticeDao
 * @Description: 通知信息数据层
 * @author: skyeye云系列--卫志强
 * @date: 2024/4/24 14:31
 * @Copyright: 2023 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface NoticeDao extends SkyeyeBaseMapper<Notice> {
}
